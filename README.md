# SAMUEL 2020 - Raspberry Pi

Projetos com Raspberry Pi

Hardware: Raspberry Pi 4b

Distro: Raspbian

<br>

# Conteúdos
1. [Iniciando no Raspberry Pi](#raspberry)
2. [Referências](#ref)

<br>

# Iniciando no Raspberry Pi
<a name="raspberry"></a>

Seguir tutoriais nas [referências](#ref).

<br>

A melhor configuração de tela/resolução que funcionou pra mim foi: [config.txt](https://gitlab.com/samucamp/samuel-2020-raspberry-pi/-/blob/master/config.txt)

Se encontra em /boot/config.txt


<br>

Sugestão para uso do UFW:

Exemplos de regras para SSH, VNC, mude o IP (conexão local) e porta conforme seu uso.
```bash
sudo ufw allow from 12.34.56.78 to any port 22 proto tcp
sudo ufw allow from 12.34.56.78 to any port 5900 proto tcp
```

Aconselho instalar Fail2ban também.

<br>

Pode ser útil:

[SAMUEL 2020 - Linux Ubuntu](https://gitlab.com/samucamp/samuel-2020-linux-ubuntu)

[SAMUEL 2020 - Python](https://gitlab.com/samucamp/samuel-2020-python)

<br>

# Referências
<a name="ref"></a>

> https://www.raspberrypi.org/downloads/raspbian/

> https://projects.raspberrypi.org/en/projects/raspberry-pi-getting-started

> https://www.raspberrypi.org/documentation/configuration/config-txt/README.md

> https://projects.raspberrypi.org/en/projects/raspberry-pi-using

> https://www.raspberrypi.org/documentation/raspbian/updating.md

> https://www.raspberrypi.org/documentation/configuration/security.md

> https://www.raspberrypi.org/documentation/linux/usage/users.md

> https://magpi.raspberrypi.org/articles/ssh-remote-control-raspberry-pi

> https://magpi.raspberrypi.org/articles/vnc-raspberry-pi